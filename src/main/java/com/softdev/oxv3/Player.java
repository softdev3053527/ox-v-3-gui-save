/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.oxv3;

import java.io.Serializable;

/**
 *
 * @author slmfr
 */
public class Player implements Serializable{
    private char symbol;
    private int loss;
    private int win;
    private int draw;
    
    public Player(char symbol){
        this.symbol = symbol;
        this.win = 0;
        this.loss = 0;
        this.draw = 0;
    }
    
    public char getSymbol(){
        return symbol;
    }
    
    public int getWin(){
        return win;
    }
    
    public int getLoss(){
        return loss;
    }
    public int getDraw(){
        return draw;
    }
    
    public void countWin(){
        this.win++;
    }
    
    public void countLoss(){
        this.loss++;
    }
    
    public void countDraw(){
        this.draw++;
    }
        
    public String toString(){
    
    return "Player "+ this.symbol + " Win = " + this.win + " Loss = " + this.loss + " Draw = " + this.draw; 
    }
    
}


